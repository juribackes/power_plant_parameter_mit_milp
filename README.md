# Power_Plant_Parameter_mit_MILP


Predicting power plant parameter from historical behaviour.

------------------------------------------------------------------------------------

## Installation
First step is to download/clone this repository.

If you are using PyCharm and no interpreter can be found, set up a new interpreter
with the newest 3.7 Python version. Make sure the Project Folder 'venv' is empty or non
existing.

In order to run the project following modules have to be installed. This can be done using 
pip or another module installer of your choice. 

   1. deap (1.3.1)
   2. matplotlib (3.2.1)
   3. pandas (1.0.3 or when running into dll errors use the 1.0.1 version)
   4. pyomo (5.5.9)
   5. xlrd (1.2.0)

Maybe newer module versions also work for this project.

Depending on the system you're running, consider to (re-)install the ipopt-solver manually.
A guide on how to set up the ipopt-solver properly can be found here:
https://github.com/matthias-k/cyipopt. A faster way is to download a the solver binary from 
https://www.coin-or.org/download/binary/Ipopt/.



------------------------------------------------------------------------------------

## Solving the bilevel Problem

### The inner problem

The inner problem is solved within the unit_commitment.py using the Pyomo library (https://www.pyomo.org/). 
Goal of the unit commitment problem is to find the optimal power output schedule regarding different economical factors. 
Specifics about the data are stated in the Data section. In the current state, the model is realized as a quadratic 
Mixed integer Problem.

The method `power_plant_model(data=data)` returns an abstract Pyomo model.

```
model = power_plant_model(data=data)
```

Instances of this model, can be solved using the ipopt-solver (https://coin-or.github.io/coinbrew/). 
The solver has to be in the project directory. 

```
model_instance = model.create_instance()
SolverFactory('ipopt').solve(model_instance)
```

Different parameters (`efficiency`, `start_up_cost`, `fixed_fixed_operational_cost`, `var_fixed_operational_cost`) 
can be changed  before solving.

```
efficiency = 0.5
model_instance.efficiency.value = efficiency 
```

### The outer problem

The outer problem, i.e. finding the parameters 
(efficiency, start up cost, fixed and variable operational costs) which lead to the minimal error between unit_commitment 
and the historical observation. 
It is solved using a genetic algorithm.
The deap library is used for this (https://deap.readthedocs.io/en/master/). The setup for the algorithm is located in 
evolutionary_algorithm.py within the ea class.

Two point crossover and Gaussian Mutation are used as algorithm operators, because they performed the best.

Default values:

   1. Mutation probability:    10% - 20%
   2. Crossover probability:   85% - 95%
   3. Population size:          50
   4. Number of generations:   400
    
While the algorithm is running, information about the generation and best fitting individual is stored in ea_data.csv 
and ea_data_pop.csv.

A single individual is build as followed [efficiency, start up cost, fixed operational costm variable operational cost]

------------------------------------------------------------------------------------

## Predicting power plant parameter

From config_n_run.py the algorithm can now be initialized and started. 
Furthermore, the most important parameters of the algorithm, as well as the boundaries of the power plant parameters 
can be changed here.

```
from evolutionary_algorithm import ea

data = pd.read_excel('data/example_data.xlsx')

evol_algorithm = ea(data=data)

evol_algorithm.POP_SIZE = 50

# real_value = lower bound + (x * upper_bound)  -  x between 0 and 1
# in this example efficiency is between 0.3 and 0.6
evol_algortihm.efficiency_bounds = 0.3, 0.3

evol_algorithm.start()
```

Make sure that the parameter bounds are specified as tuples, as shown in the code 
example.

Every generation some information is provided. The genes are values between 0..1 and have to be 'denormalized' 
accordingly `real value = lower bound + (gene value * upper bound))`

While the generic algorithm is running, the data can be visualized by starting the plot.py script. 

------------------------------------------------------------------------------------

## Data 

To predict the different power plant parameters following data has to be provided 
in .csv or .xlsx format:

   1. Time steps (0...n)
   2. Maximum Export Limit [MW]
   3. Electricity price [€/MWh]
   4. Fuel price [€/MWh(fuel)]
   5. Emission Price [€/tCO2]
   6. Emission Factor [tCO2/MWh(fuel)]
   7. Up and down ramp limits [MW/h] 
   8. Historical data [MW]

More information about different ramp/emission parameters can be found in the parameter folder.
Example data files can be found in the data folder.

```
data = pd.read_excel('data/example_data.xlsx')
```

Time varying data has to be complete (in the sense, that no time step should be missing) 
and have to be all the same length.

------------------------------------------------------------------------------------

## Visualization

### plot.py

This script can be run to visualize the behaviour of the genetic algorithm. In order to do that it reads
the data located in the ea_data.csv and ea_data_pop.csv files. So it can be run simultaneously to the 
genetic algorithm or afterwards.

### visualize_unit_commitment.py (former concrete_model.py)

This script is to verify or understand the solver output of different model parameters.
The output shows the predicted behaviour and the historical unit commitment.
In addition to that, also the resulting profit and the binary variables running/starting are shown.

```
instance.efficiency.value = example_efficiency
instance.start_up_cost.value = example_start_up_cost
instance.fixed_operational_cost.value = example_fixed_operational_cost
instance.variable_operational_cost.value = example_variable_operational_cost
```

The code example shows how to change the model parameter.



