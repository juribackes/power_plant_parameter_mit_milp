from evolutionary_algorithm import ea
import pandas as pd

data = pd.read_excel('data/bergkamen_0219.xlsx')

evol_algorithm = ea(data=data)

evol_algorithm.POP_SIZE = 50
evol_algorithm.MUTPB = 0.15
evol_algorithm.CXPB = 0.8

evol_algorithm.efficiency_bounds = 0.3, 0.25

evol_algorithm.start()
