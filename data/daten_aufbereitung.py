import pandas as pd
from datetime import date, datetime, timedelta

string_Zeitstempel = 'Zeitstempel'
string_Value = '€/MWh'
string_fileName = 'Kohlepreis'


data = pd.read_excel(string_fileName + '.xlsx')
starting_day = date(2019, 1, 1)


size = len(data[string_Zeitstempel]) - 1
dates = []
values = []

for i in range(0, size + 1):
    date = data[string_Zeitstempel][size - i]
    dates.append(date)
    value = data[string_Value][size - i]
    values.append(value)

new_dates = []
new_values = []

value = 0
k = 0
for i in range(0, 365):
    if k < size + 1:
        if dates[k].day is starting_day.day and dates[k].month is starting_day.month:
            value = values[k]
            k += 1
    for j in range(0, 24):
        new_dates.append(starting_day)
        new_values.append(value)
    starting_day += timedelta(days=1)

d = {string_Zeitstempel: new_dates, string_Value: new_values}
excel_ready = pd.DataFrame(d)


excel_ready.to_excel(string_fileName + '_korrekt.xlsx')
