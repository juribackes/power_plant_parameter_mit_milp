import csv
import random
from deap import base
from deap import creator
from deap import tools
from pyomo.opt import SolverFactory
from unit_commitment import power_plant_model
from datetime import datetime

"""
The outer part of the bilevel problem is located here. A generic algorithm is used to find the parameters 
leading to the optimal minimum error between the historical data and the predicted unit commitment.
"""



class ea(object):
    def __init__(self, data):

        self.data = data

        self.POP_SIZE = 50      # Population Size >> default 50
        self.NGEN = 100 * 4     # Number of  Generations >> default 400
        self.CXPB = 0.9         # Crossover Probability
        self.MUTPB = 0.05       # Mutation Probability
        self.tourn_size = 4     # number of individuals selected for each tournament

        # parameter bounds
        # we want the genes to be all in the same range from 0 to 1
        # efficiency is the only exception, is not allowed to be 0
        self.efficiency_bounds = 0.3, 0.25
        self.start_up_bounds = 0, 250
        self.fixed_op_bounds = 0, 30
        self.var_op_bounds = 0, 25

        # keeping track of already 'known' individuals and total number of solves
        self.individualIndividuals = []
        self.solved_individualIndividuals = []
        self.indNmr = 0

        # unit commitment milp model
        self.model = power_plant_model(data=data)
        self.instance = self.model.create_instance()

        # toolbox for the generic algorithm
        self.toolbox = self.setup()

    """ 
    Set up the evolutionary algorithm  
        
        Creating different generic algorithm operators like mutation and crossover function. Also the individual is
        defined here.
        
        returns the deap toolbox
    """
    def setup(self):
            # number of data points
            data_points = len(self.data['time']) - 1

            # Create Individual and defining FitnessMin Class
            creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
            creator.create("Individual", list, fitness=creator.FitnessMin)

            # Create toolbox for Individuals and Operators
            toolbox = base.Toolbox()

            # Setting variable bounds (Attribute Generators)
            # Efficiency (is not allowed to be 0)
            toolbox.register("attr_float_e", random.uniform, 0.0000001, 1)
            # Start up cost
            toolbox.register("attr_float_s", random.uniform, 0.0, 1)
            # Fixed OP cost
            toolbox.register("attr_float_f", random.uniform, 0.0, 1)
            # Variable OP cost
            toolbox.register("attr_float_v", random.uniform, 0.0, 1)
            # Define Individual
            toolbox.register("individual", tools.initCycle, creator.Individual, (toolbox.attr_float_e,
                                                                                 toolbox.attr_float_s,
                                                                                 toolbox.attr_float_f,
                                                                                 toolbox.attr_float_v), n=1)

            # Registering generic Operators required for the evolution
            toolbox.register("mate", tools.cxTwoPoint)
            toolbox.register("mutate", tools.mutGaussian, mu=0, sigma=0.2, indpb=0.3)
            toolbox.register("select", tools.selTournament, tournsize=self.tourn_size)
            toolbox.register("evaluate", self.evaluate)

            # Decorating the mate/mutate Operators
            toolbox.decorate("mate", self.check_bounds())
            toolbox.decorate("mutate", self.check_bounds())

            documents = ['data/ea_data.csv', 'data/ea_data_pop.csv']

            for doc in documents:
                with open(doc, 'w+') as fd:
                    writer = csv.writer(fd)
                    line = ['gen','eff','start','fix','var','err']
                    writer.writerow(line)

            return toolbox

    """ 
    Evaluate a single individual  
        
        Takes an individual as input and 'denormalizes' the different genes. Checks if the exact individual was already 
        solved. 
        If not, run the unit commitment problem with the specific parameters and safe the error between 
        historical and predicted unit commitment.
        Else, the error is looked up.
        
        returns the error
    """
    def evaluate(self, individual):
        self.indNmr += 1

        individual = self.denormalizeInd(individual)

        ind = (individual['efficiency'], individual['start_up_cost'],
               individual['fixed_op_cost'], individual['var_op_cost'])

        # Checking if the individual is known
        if ind in self.individualIndividuals:
            # getting the known solution
            index = self.individualIndividuals.index(ind)
            error = self.solved_individualIndividuals[index]

            # self.print_time()
            # print(self.indNmr, ':', ind, 'is identical to [', index, '] error:', error)

            # the saved error is already a tuple
            return error

        # if the individual is not known
        else:

            # Setting parameter Values
            self.instance.efficiency.value = individual['efficiency']
            self.instance.start_up_cost.value = individual['start_up_cost']
            self.instance.fixed_operational_cost.value = individual['fixed_op_cost']
            self.instance.variable_operational_cost.value = individual['var_op_cost']

            # Solving the Unit-Commitment Problem
            try:
                SolverFactory('ipopt').solve(self.instance)

                # Calculating error
                error = sum((self.instance.power[t]() - self.data['hist'][t]) ** 2 for t in self.instance.time),
                # self.print_time()
                # print(self.indNmr, ':', ind, 'error:', error)

                # the individual is now known
                self.individualIndividuals.append(ind)
                self.solved_individualIndividuals.append(error)

                # returned error is a tuple
                return error
            # Sometimes an unknown solver error occurs
            except ValueError:
                print(self.indNmr, ": Solving error at:", ind)
                return 65000000,

    """ 
    Decorator controlling parameter bounds  
    
        This is called for each individual to check if the genes are still inside of the parameter bounds. 
        If not, the parameter will be changed accordingly.
        Else, Nothing happens
    """
    def check_bounds(self):
        def decorator(func):
            def wrapper(*args, **kargs):
                offspring = func(*args, **kargs)
                for child in offspring:
                    if child[0] > 1:  # checking efficiency bounds
                        child[0] = 1
                    elif child[0] < 0.0000001:
                        child[0] = 0.0000001
                    if child[1] > 1.5:  # checking start up cost bounds
                        child[1] = 1.5
                    elif child[1] < 0:
                        child[1] = 0
                    if child[2] > 1.5:  # checking fixed operational cost bounds
                        child[2] = 1.5
                    elif child[2] < 0:
                        child[2] = 0
                    if child[3] > 1.5:  # checking variable operational cost bounds
                        child[3] = 1.5
                    elif child[3] < 0:
                        child[3] = 0
                return offspring
            return wrapper
        return decorator

    """ 
    Saving the best individual and the whole population for each generation 
    
        The information are saved for debugging purposes. But they can be visualized with the plot.py script.    
    """
    def safe(self, generation, population):

        nmbr1 = tools.selBest(population, 1)
        nmbr1 = nmbr1[0]
        normalNmbr1 = self.denormalizeInd(nmbr1)

        # saving the single best individual of the generation
        line = [generation, normalNmbr1['efficiency'], normalNmbr1['start_up_cost'],
                normalNmbr1['fixed_op_cost'], normalNmbr1['var_op_cost'], nmbr1.fitness.values[0]]
        with open('data/ea_data.csv', 'a') as fd:
            writer = csv.writer(fd)
            writer.writerow(line)

        # saving the whole population
        with open('data/ea_data_pop.csv', 'a') as fd:
            writer = csv.writer(fd)
            for ind in population:
                normalInd = self.denormalizeInd(ind)
                line = [generation, normalInd['efficiency'], normalInd['start_up_cost'],
                        normalInd['fixed_op_cost'], normalInd['var_op_cost'], ind.fitness.values[0]]
                writer.writerow(line)

    def denormalizeInd(self, individual):

        # denormalizing the parameter
        efficiency = self.efficiency_bounds[0] + (individual[0] * self.efficiency_bounds[1])
        start_up_cost = self.start_up_bounds[0] + (individual[1] * self.start_up_bounds[1])
        fixed_op_cost = self.fixed_op_bounds[0] + (individual[2] * self.fixed_op_bounds[1])
        var_op_cost = self.var_op_bounds[0] + (individual[3] * self.var_op_bounds[1])

        denormalizedInd = {'efficiency': efficiency, 'start_up_cost': start_up_cost,
                           'fixed_op_cost': fixed_op_cost, 'var_op_cost': var_op_cost}

        return denormalizedInd

    """
    Printing out the current status and information about the evolutionary algorithm
    """
    def print_information(self, population):

        # Gather all the fitnesses in one list and print the stats
        fits = [ind.fitness.values[0] for ind in population]

        # Printing Information
        length = len(population)
        mean = sum(fits) / length
        sum2 = sum(x * x for x in fits)
        std = abs(sum2 / length - mean ** 2) ** 0.5

        print("  Min %s" % min(fits))
        print("  Max %s" % max(fits))
        print("  Avg %s" % mean)
        print("  Std %s" % std)
        selected = tools.selBest(population, 3)

        for ind in selected:
            print(self.denormalizeInd(ind))

        self.print_time

    """
    Printing out the current time
    """
    def print_time(self):
        # printing time
        now = datetime.now()

        current_time = now.strftime("%H:%M:%S")
        print("Time =", current_time)

    """
    Starting the evolutionary algorithm
        
        Where the actual magic happens!
    """
    def start(self):
        # Creating the population
        self.toolbox.register("population", tools.initRepeat, list, self.toolbox.individual)
        pop = self.toolbox.population(n=self.POP_SIZE)

        # Evaluate the entire population
        fitnesses = list(map(self.toolbox.evaluate, pop))
        for ind, fit in zip(pop, fitnesses):
            ind.fitness.values = fit

        # Extracting all the fitnesses
        fits = [ind.fitness.values[0] for ind in pop]

        # keeping track of the number of generations
        g = 0

        # stops when sse smaller than 10000 or number of max generations exceeded
        while min(fits) > 10000 and g < self.NGEN:
            # A new generation
            g = g + 1
            print("-- Generation %i --" % g)

            # Select the next generation individuals
            offspring = self.toolbox.select(pop, self.POP_SIZE)
            # Clone the selected individuals
            offspring = list(map(self.toolbox.clone, offspring))

            # Apply crossover and mutation on the offspring
            for child1, child2 in zip(offspring[::2], offspring[1::2]):
                if random.random() < self.CXPB:
                    self.toolbox.mate(child1, child2)
                    del child1.fitness.values
                    del child2.fitness.values

            for mutant in offspring:
                if random.random() < self.MUTPB:
                    self.toolbox.mutate(mutant)
                    del mutant.fitness.values

            # Evaluate the individuals with an invalid fitness
            invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
            fitnesses = map(self.toolbox.evaluate, invalid_ind)
            for ind, fit in zip(invalid_ind, fitnesses):
                ind.fitness.values = fit

            pop[:] = offspring

            # Print general information about the generation + the top 3 individuals
            self.print_time()
            self.print_information(population=pop)
            # Saving the generation
            self.safe(generation=g, population=pop)
