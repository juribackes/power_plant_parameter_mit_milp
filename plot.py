
from itertools import count
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation


index = count()

fig = plt.figure()
fig1 = plt.figure()

ax11 = fig.add_subplot(3, 2, 1)
ax12 = fig.add_subplot(3, 2, 2)
ax21 = fig.add_subplot(3, 2, 3)
ax22 = fig.add_subplot(3, 2, 4)
ax31 = fig.add_subplot(3, 2, 5)

bx11 = fig1.add_subplot(2, 2, 1)
bx12 = fig1.add_subplot(2, 2, 2)
bx21 = fig1.add_subplot(2, 2, 3)
bx22 = fig1.add_subplot(2, 2, 4)

plots = [ax11, ax12, ax21, ax22, ax31, bx11, bx12, bx21, bx22]

def animate(i):
    data = pd.read_csv('data/ea_data.csv')
    data_pop = pd.read_csv('data/ea_data_pop.csv')

    x_vals = data['gen']
    x_vals_pop = data_pop['gen']

    # clear the plots
    for plot in plots:
        plot.cla()

    # Generation to variable
    ax11.plot(x_vals, data['eff'])
    ax11.scatter(x_vals_pop, data_pop['eff'], c='gray', s=5)
    ax11.set_ylabel('Efficiency in %')
    ax11.set_xlabel('Generation')

    ax12.plot(x_vals, data['start'])
    ax12.scatter(x_vals_pop, data_pop['start'], c='gray', s=5)
    ax12.set_ylabel('Start up cost in €')
    ax12.set_xlabel('Generation')

    ax22.plot(x_vals, data['fix'])
    ax21.scatter(x_vals_pop, data_pop['fix'], c='gray', s=5)
    ax21.set_ylabel('Fixed operational cost in €/h')
    ax21.set_xlabel('Generation')

    ax21.plot(x_vals, data['var'])
    ax22.scatter(x_vals_pop, data_pop['var'], c='gray', s=5)
    ax22.set_ylabel('Variable operational cost €/MWh')
    ax22.set_xlabel('Generation')

    ax31.plot(x_vals, data['err'])
    ax31.scatter(x_vals_pop, data_pop['err'], c='gray', s=5)
    ax31.set_ylabel('Error')
    ax31.set_xlabel('Generation')

    # Variable to error
    ylim = min(data['err']) - (min(data['err'])/2), 1.5 * min(data['err'])

    bx11.scatter(data_pop['eff'], data_pop['err'], c='blue', s=5)
    bx11.set_ylabel('Error')
    bx11.set_xlabel('Efficiency %')
    bx11.set_ylim(ylim)

    bx12.scatter(data_pop['start'], data_pop['err'], c='blue', s=5)
    bx12.set_ylabel('Error')
    bx12.set_xlabel('Start up cost €')
    bx12.set_ylim(ylim)

    bx21.scatter(data_pop['fix'], data_pop['err'], c='blue', s=5)
    bx21.set_ylabel('Error')
    bx21.set_xlabel('Fix operational cost €/h')
    bx21.set_ylim(ylim)

    bx22.scatter(data_pop['var'], data_pop['err'], c='blue', s=5)
    bx22.set_ylabel('Error')
    bx22.set_xlabel('Variable operational cost €/MWh')
    bx22.set_ylim(ylim)


ani = FuncAnimation(plt.gcf(), animate, interval=3000)

plt.tight_layout()
plt.show()
