from pyomo.environ import Param, Var, NonNegativeReals, Boolean, maximize, Objective, Constraint, \
    Set, AbstractModel, inequality, Expression

"""
The model of inner problem is located here.
"""


def power_plant_model(data):
    # INITIALIZE ABSTRACT MODEL
    model = AbstractModel()

    data_dict = data.to_dict()

    # READ PARAMETERS FROM DATA FILE
    # Time [h]
    model.time = Set(initialize=sorted(data['time']))
    # Maximum Export Limit (MEL) [MW]
    model.export_limit = Param(model.time)
    # Wholesale Electricity Price [€/MWh]
    model.electricity_price = Param(model.time, initialize=data_dict['eprice'], default=49.39)
    # Fuel Price [€/MWh(fuel)]
    model.fuel_price = Param(model.time, initialize=data_dict['fuelprice'], default=8.255)
    # Emissions price [€/tCO2]
    model.emission_price = Param(model.time, initialize=data_dict['emission_price'], default=22.57)
    # Emission factor [tCO2/MWh(fuel)]
    model.emission_factor = Param(initialize=data['emission_factor'].iat[0])
    # Ramping up limits [MW/h]
    model.r_up = Param(initialize=data['rup'].iat[0])
    # Ramping down limits [MW/h]
    model.r_down = Param(initialize=data['rdown'].iat[0])

    # INITIALIZE ESTIMATION PARAMETERS
    # Thermal efficiency
    model.efficiency = Param(within=NonNegativeReals, mutable=True)
    # Start-up costs [€]
    model.start_up_cost = Param(within=NonNegativeReals, mutable=True)
    # Fixed O&M costs [€/h]
    model.fixed_operational_cost = Param(within=NonNegativeReals, mutable=True)
    # Variable O&M costs [€/MWh]
    model.variable_operational_cost = Param(within=NonNegativeReals, mutable=True)

    # DECLARING VARIABLES
    # Power plant output [MW]
    model.power = Var(model.time, bounds=(0, data['exportlim'].iat[0]))
    # Binary variable, 1 if power plant is running
    model.running = Var(model.time, domain=Boolean, initialize=1)
    # Binary variable, 1 if power plant has started
    model.starting = Var(model.time, domain=Boolean, initialize=0)
    # Binary variable, 1 if power plant is shutting down
    model.shutting_down = Var(model.time, domain=Boolean, initialize=0)

    """ 
    Define Profit Expression 

        Calculating the profit for every hour.

            Earnings:   power[t] * electricity price[t]

            Costs:      power[t] * variable operational cost
                        power[t] * fuel price[t] / efficiency
                        power[t]* (emission_factor * emission price[t]) / efficiency

                        fixed operational cost
                        start up cost 
    """
    
    # Profit [€]
    def profit_fixed_rule(m, t):
        return m.power[t] * (m.electricity_price[t]
                                            - m.variable_operational_cost
                                            - (m.fuel_price[t] / m.efficiency)
                                            - ((m.emission_factor * m.emission_price[t]) * (1 / m.efficiency))) \
               - (m.running[t] * m.fixed_operational_cost) \
               - (m.starting[t] * m.start_up_cost)
    model.profit = Expression(model.time, rule=profit_fixed_rule)

    """
    Setting the binary variable <running>
        
        1 when power plant is running, 0 otherwise
    """

    def running_rule1(m, t):
        return m.power[t] * (1 - m.running[t]) == 0

    model.running_constr1 = Constraint(model.time, rule=running_rule1)

    def running_rule2(m, t):
        if t > 0:
            return m.running[t - 1] - m.running[t] + m.starting[t] - m.shutting_down[t] == 0
        else:
            return Constraint.Skip

    model.running_constr2 = Constraint(model.time, rule=running_rule2)

    """
    Setting the binary variable <starting>
        
        1 when the power plant is starting, 0 otherwise
    """

    def starting_rule(m, t):
        if t > 0:
            return m.starting[t] <= (m.running[t] - m.running[t - 1] + 1) / 2
        else:
            return Constraint.Skip

    model.starting_constr = Constraint(model.time, rule=starting_rule)

    """
    Setting the binary variable <shutting_down>
        
        1 when the power plant is shutting down, 0 otherwise
    """

    def shutdown_rule(m, t):
        if t > 0:
            return m.shutting_down[t] >= (m.running[t] - m.running[t - 1] - 1) / 2
        else:
            return Constraint.Skip

    model.shutdown_constr = Constraint(model.time, rule=shutdown_rule)

    """
    Setting the ramping limits
        
        -ramp down limit <= dpower/dt <= ramp up limit
    """

    def ramping_limits(m, t):
        if t < len(m.time) - 1:
            return inequality(-m.r_down, m.power[t + 1] - m.power[t], m.r_up)
        else:
            return Constraint.Skip

    model.ramping_limits_constr1 = Constraint(model.time, rule=ramping_limits)

    """ 
    OBJECTIVE
        
        maximum profit
    """

    def obj_rule(m):
        return sum(m.profit[t] for t in m.time)

    model.obj = Objective(rule=obj_rule, sense=maximize)

    return model
# ---------------------------------------------------------------------------------------------------------------------
