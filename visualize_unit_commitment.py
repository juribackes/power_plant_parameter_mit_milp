from unit_commitment import power_plant_model
from pyomo.opt import SolverFactory
import matplotlib.pyplot as plt
import pandas as pd

data = pd.read_excel('data/bergkamen_0119.xlsx')

model = power_plant_model(data)
instance = model.create_instance()

instance.efficiency.value =0.3611318740222411
instance.start_up_cost.value = 186.90600146665392
instance.fixed_operational_cost.value = 17.632310420913427
instance.variable_operational_cost.value = 18.218766970614674

SolverFactory('ipopt').solve(instance)

power_list = [instance.power[t]() for t in instance.time]
running_list = [50 * instance.running[t]() - 50 for t in instance.time]
starting_list = [50 * instance.starting[t]() - 100 for t in instance.time]
profit_list = [instance.profit[t]() * 1 / 100 for t in instance.time]
plt.plot(running_list, label='running')
plt.plot(starting_list, label='starting')
plt.plot(data['hist'], label='hist')
plt.plot(power_list, label='pow')
plt.plot(profit_list, label='profit')
legend = plt.legend(loc='best')
err = sum((instance.power[t]() - data['hist'][t]) ** 2 for t in instance.time)

print("{:.5e}".format(err))
print(err)

plt.show()
